# pytictactoe

a simple tictactoe game in Python/PyQt.

made in Qt, just bc i am familar with it

it was not copied from any on-line tutorial, and took me few hours to make.

draws are not implemented EDIT: they are, but just the game crashes :C

the enemy algorithm is quite dumb, but it actually defeated me some times. EDIT: if you want, you can try the Impossible difficulty, but remember that it's IMPOSSIBLE

### Dependiences and running

The only dependiences are Python, Qt5 and PyQt5

once you have them installed, just run the pyttt.py script

Optionally you can run the old version with:

    curl -k -L https://codeberg.org/attachments/b727c037-2ad2-4acf-af9d-7974ff907eb5 | python3
