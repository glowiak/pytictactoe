#!/usr/bin/env python3
import os, sys, random
try:
    from PyQt5.QtCore import *
    from PyQt5.QtGui import *
    from PyQt5.QtWidgets import *
except:
    print("Looks like you don't have PyQt installed, so I will install it for you.")
    os.system("pip install PyQt5")
    from PyQt5.QtCore import *
    from PyQt5.QtGui import *
    from PyQt5.QtWidgets import *

class App(QMainWindow):
    def __init__(self):
        super().__init__()
        self.w = QWidget()
        self.w.setWindowTitle("pyTicTacToe")
        self.w.setGeometry(100,100,500,400)
        
        # Menu
        self.well = QLabel(self.w)
        self.well.setPixmap(QPixmap('title.png').scaledToHeight(300).scaledToWidth(300))
        self.well.move(100,0)
        self.vcButt = QPushButton(self.w)
        self.vcButt.setText("Player vs Computer")
        self.vcButt.resize(200,100)
        self.vcButt.move(10,110)
        self.vcButt.clicked.connect(self.VsComputer)
        self.vpButt = QPushButton(self.w)
        self.vpButt.setText("Player vs Player")
        self.vpButt.resize(200,100)
        self.vpButt.move(290,110)
        
        # Difficulty arrays and button
        self.difficulty = 1
        self.Difficulty = [ "", "Normal", "Impossible" ]
        # 1 - normal
        # 2 - impossible
        
        self.dButt = QPushButton(self.w)
        self.dButt.setText(self.Difficulty[self.difficulty])
        self.dButt.resize(100,40)
        self.dButt.move(5,355)
        self.dButt.clicked.connect(self.changeDiff)
        
        self.w.show()

    def changeDiff(self):
        if self.difficulty == 1:
            self.difficulty = 2
        elif self.difficulty == 2:
            self.difficulty = 1
        
        # print the difficulty
        print(f"Difficulty: {self.Difficulty[self.difficulty]}")
        
        # reload the button text
        self.dButt.setText(self.Difficulty[self.difficulty])

    def VsComputer(self):
        print("Mode: vs computer")
        self.well.hide()
        self.vcButt.hide()
        self.vpButt.hide()
        self.w.setGeometry(100,100,300,330)
        
        self.turn = "X"
        self.gameBlocked = False
        self.Butt = [ "", "", "", "", "", "", "", "", "", "" ]
        self.Placed = [ "", "", "", "", "", "", "", "", "", "" ]

        
        # top line
        startPosX = 0
        startPosY = 0
        for i in range(3):
            j = i + 1
            self.Butt[j] = QPushButton(self.w)
            self.Butt[j].move(startPosX, startPosY)
            self.Butt[j].resize(100,100)
            startPosX = startPosX + 100
        
        # middle line
        startPosX = 0
        startPosY = 100
        j = 4
        for i in range(3):
            self.Butt[j] = QPushButton(self.w)
            self.Butt[j].move(startPosX, startPosY)
            self.Butt[j].resize(100,100)
            startPosX = startPosX + 100
            j = j + 1
        
        # bottom line
        startPosX = 0
        startPosY = 200
        j = 7
        for i in range(3):
            self.Butt[j] = QPushButton(self.w)
            self.Butt[j].move(startPosX, startPosY)
            self.Butt[j].resize(100,100)
            startPosX = startPosX + 100
            j = j + 1
        
        # turn signalization
        self.turnText = QLabel(self.w)
        self.turnText.move(5,310)
        self.turnText.setText(f"Turn: {self.turn}")
        
        # who win
        self.winText = QLabel(self.w)
        self.winText.move(190, 310)
        self.winText.setText("Nobody has won!") # X/O has won!
        
        # set button actions
        self.Butt[1].clicked.connect(self.Butt1Click)
        self.Butt[2].clicked.connect(self.Butt2Click)
        self.Butt[3].clicked.connect(self.Butt3Click)
        self.Butt[4].clicked.connect(self.Butt4Click)
        self.Butt[5].clicked.connect(self.Butt5Click)
        self.Butt[6].clicked.connect(self.Butt6Click)
        self.Butt[7].clicked.connect(self.Butt7Click)
        self.Butt[8].clicked.connect(self.Butt8Click)
        self.Butt[9].clicked.connect(self.Butt9Click)
        
        # the reset button
        self.reB = QPushButton(self.w)
        self.reB.setText("Reset")
        self.reB.resize(100,20)
        self.reB.move(85,310)
        self.reB.clicked.connect(self.resetGame)
        
        # show everything
        for i in range(9):
            j = i + 1
            self.Butt[j].show()
            
        self.turnText.show()
        self.winText.show()
        self.reB.show()

    def Butt1Click(self):
        if self.turn == "X":
            if self.Placed[1] != "X" and self.Placed[1] != "O" and self.gameBlocked != True:
                self.Butt[1].setStyleSheet("background-image: url(x.png);")
                self.Placed[1] = "X"
                self.setTurn("O")
                self.enemyBot()
                print("Pressed 1")
    
    def Butt2Click(self):
        if self.turn == "X":
            if self.Placed[2] != "X" and self.Placed[2] != "O" and self.gameBlocked != True:
                self.Butt[2].setStyleSheet("background-image: url(x.png);")
                self.Placed[2] = "X"
                self.setTurn("O")
                self.enemyBot()
                print("Pressed 2")
        
    def Butt3Click(self):
        if self.turn == "X":
            if self.Placed[3] != "X" and self.Placed[3] != "O" and self.gameBlocked != True:
                self.Butt[3].setStyleSheet("background-image: url(x.png);")
                self.Placed[3] = "X"
                self.setTurn("O")
                self.enemyBot()
                print("Pressed 3")
    
    def Butt4Click(self):
        if self.turn == "X":
            if self.Placed[4] != "X" and self.Placed[4] != "O" and self.gameBlocked != True:
                self.Butt[4].setStyleSheet("background-image: url(x.png);")
                self.Placed[4] = "X"
                self.setTurn("O")
                self.enemyBot()
                print("Pressed 4")
    
    def Butt5Click(self):
        if self.turn == "X":
            if self.Placed[5] != "X" and self.Placed[5] != "O" and self.gameBlocked != True:
                self.Butt[5].setStyleSheet("background-image: url(x.png);")
                self.Placed[5] = "X"
                self.setTurn("O")
                self.enemyBot()
                print("Pressed 5")
    
    def Butt6Click(self):
        if self.turn == "X":
            if self.Placed[6] != "X" and self.Placed[6] != "O" and self.gameBlocked != True:
                self.Butt[6].setStyleSheet("background-image: url(x.png);")
                self.Placed[6] = "X"
                self.setTurn("O")
                self.enemyBot()
                print("Pressed 6")
    
    def Butt7Click(self):
        if self.turn == "X":
            if self.Placed[7] != "X" and self.Placed[7] != "O" and self.gameBlocked != True:
                self.Butt[7].setStyleSheet("background-image: url(x.png);")
                self.Placed[7] = "X"
                self.setTurn("O")
                self.enemyBot()
                print("Pressed 7")
    
    def Butt8Click(self):
        if self.turn == "X":
            if self.Placed[8] != "X" and self.Placed[8] != "O" and self.gameBlocked != True:
                self.Butt[8].setStyleSheet("background-image: url(x.png);")
                self.Placed[8] = "X"
                self.setTurn("O")
                self.enemyBot()
                print("Pressed 8")
    
    def Butt9Click(self):
        if self.turn == "X":
            if self.Placed[9] != "X" and self.Placed[9] != "O" and self.gameBlocked != True:
                self.Butt[9].setStyleSheet("background-image: url(x.png);")
                self.Placed[9] = "X"
                self.setTurn("O")
                self.enemyBot()
                print("Pressed 9")

    def setTurn(self, xo):
        self.turn = xo
        self.turnText.setText(f"Turn: {xo}")
        print(f"{xo}'s turn!")
        self.v = self.victoryCheck()
        if self.v == "X":
            self.winText.setText("X has won!")
            self.gameBlocked = True
            print("X has won!")
        if self.v == "O":
            self.winText.setText("O has won!")
            self.gameBlocked = True
            print("O has won!")

    def enemyBot(self):
        if self.gameBlocked:
            # do nothing
            print("END GAME")
        else:
            rnd = random.randint(1,9)
            if self.Placed[rnd] != "X" and self.Placed[rnd] != "O":
                self.Butt[rnd].setStyleSheet("background-image: url(o.png);")
                self.Placed[rnd] = "O"
                self.setTurn("X")
            else:
                self.enemyBot() # if randint fails, rerun the bot
            if self.difficulty == 2:
                rnd = random.randint(1,9)
                if self.Placed[rnd] != "X" and self.Placed[rnd] != "O":
                    self.Butt[rnd].setStyleSheet("background-image: url(o.png);")
                    self.Placed[rnd] = "O"
                    self.setTurn("X")
                else:
                    self.enemyBot() # if randint fails, rerun the bot

    def victoryCheck(self):
        # X
        if self.Placed[1] == "X" and self.Placed[2] == "X" and self.Placed[3] == "X":
            print("Combination: 123")
            return("X")
        if self.Placed[1] == "X" and self.Placed[4] == "X" and self.Placed[7] == "X":
            print("Combination: 147")
            return("X")
        if self.Placed[1] == "X" and self.Placed[5] == "X" and self.Placed[9] == "X":
            print("Combination: 159")
            return("X")
        if self.Placed[2] == "X" and self.Placed[5] == "X" and self.Placed[8] == "X":
            print("Combination: 258")
            return("X")
        if self.Placed[3] == "X" and self.Placed[5] == "X" and self.Placed[7] == "X":
            print("Combination: 357")
            return("X")
        if self.Placed[4] == "X" and self.Placed[5] == "X" and self.Placed[6] == "X":
            print("Combination: 456")
            return("X")
        if self.Placed[3] == "X" and self.Placed[6] == "X" and self.Placed[9] == "X":
            print("Combination: 369")
            return("X")
        if self.Placed[7] == "X" and self.Placed[8] == "X" and self.Placed[9] == "X":
            print("Combination: 789")
            return("X")
        # O
        if self.Placed[1] == "O" and self.Placed[2] == "O" and self.Placed[3] == "O":
            print("Combination: 123")
            return("O")
        if self.Placed[1] == "O" and self.Placed[4] == "O" and self.Placed[7] == "O":
            print("Combination: 147")
            return("O")
        if self.Placed[1] == "O" and self.Placed[5] == "O" and self.Placed[9] == "O":
            print("Combination: 159")
            return("O")
        if self.Placed[2] == "O" and self.Placed[5] == "O" and self.Placed[8] == "O":
            print("Combination: 258")
            return("O")
        if self.Placed[3] == "O" and self.Placed[5] == "O" and self.Placed[7] == "O":
            print("Combination: 357")
            return("O")
        if self.Placed[4] == "O" and self.Placed[5] == "O" and self.Placed[6] == "O":
            print("Combination: 456")
            return("O")
        if self.Placed[3] == "O" and self.Placed[6] == "O" and self.Placed[9] == "O":
            print("Combination: 369")
            return("O")
        if self.Placed[7] == "O" and self.Placed[8] == "O" and self.Placed[9] == "O":
            print("Combination: 789")
            return("O")

    def resetGame(self):
        for i in range(9):
            j = i + 1
            self.Butt[j].setStyleSheet("background-image: none;")
            self.Placed[j] = ""
        self.setTurn("X")
        self.gameBlocked = False
        self.winText.setText("Nobody has won!")
        print("RESET!")

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
